import SVGO from "svgo";
import fs from "fs";
import { promisify } from "util";

const writeFile = promisify(fs.writeFile);

export const element = (tag) => (params, children = []) => {
  return {
    tag, params, children,
  };
};

export const elementG = (tag) => (children, params = {}) => {
  return {
    tag, params, children,
  };
};


export const circle = element('circle');
export const rect = element('rect');
export const polygon = element('polygon');
export const line = element('line');
export const g = elementG('g');
export const text = element('text');

export const move = (element, x, y) => {
  return g([element], {
    transform: `translate(${x},${y})`,
  })
};

export const scale = (element, x = 1, y = x) => {
  return g([element], {
    transform: `scale(${x},${y})`,
  })
};

export const mask = (e, mask) => {
  return g([
    element('mask')({ id: 'mask1' }, [mask]),
    g([e], { mask: 'url(#mask1)' }),
  ]);
};

export const compile = (el) => {
  if (typeof el === 'string') return el;
  const { tag, params, children } = el;
  const at = Object.keys(params).map(x => `${x}="${params[x]}"`).join(' ');
  if (children.length === 0) {
    return `<${tag} ${at}/>`;
  }
  const ch = children.map(x => compile(x)).join('');
  return `<${tag} ${at}>${ch}</${tag}>`;
};

export const grid = () => {
  return g([
    ...[...Array(10).keys()].map(i => {
      return line({ x1: i*10, y1: 0, x2: i*10, y2: 100});
    }),
    ...[...Array(10).keys()].map(i => {
      return line({ y1: i*10, x1: 0, y2: i*10, x2: 100});
    }),
  ], {
    style:`stroke-width: 1; stroke: #0f0;`
  });
};

export const svg = (element, { viewBox = '0 0 100 100' }) => 
`<?xml version="1.0" standalone="no"?>
<svg viewBox="${viewBox}" version="1.1" xmlns="http://www.w3.org/2000/svg">${
  compile(element)
}</svg>`;

const svgo = new SVGO({
  plugins: [{
    cleanupAttrs: true,
  }, {
    removeDoctype: true,
  },{
    removeXMLProcInst: true,
  },{
    removeComments: true,
  },{
    removeMetadata: true,
  },{
    removeTitle: true,
  },{
    removeDesc: true,
  },{
    removeUselessDefs: true,
  },{
    removeEditorsNSData: true,
  },{
    removeEmptyAttrs: true,
  },{
    removeHiddenElems: true,
  },{
    removeEmptyText: true,
  },{
    removeEmptyContainers: true,
  },{
    removeViewBox: false,
  },{
    cleanupEnableBackground: true,
  },{
    convertStyleToAttrs: true,
  },{
    convertColors: true,
  },{
    convertPathData: true,
  },{
    convertTransform: true,
  },{
    removeUnknownsAndDefaults: true,
  },{
    removeNonInheritableGroupAttrs: true,
  },{
    removeUselessStrokeAndFill: true,
  },{
    removeUnusedNS: true,
  },{
    cleanupIDs: true,
  },{
    cleanupNumericValues: true,
  },{
    moveElemsAttrsToGroup: true,
  },{
    moveGroupAttrsToElems: true,
  },{
    collapseGroups: true,
  },{
    removeRasterImages: false,
  },{
    mergePaths: true,
  },{
    convertShapeToPath: true,
  },{
    sortAttrs: true,
  },{
    removeDimensions: true,
  }]
});

export const storeSVG = async (path, element, params = {}) => {
  const r = await svgo.optimize(svg(element, params));
  await writeFile(path, r.data);
};
