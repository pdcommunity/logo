import { svg, circle, g, move, polygon, line, scale, text } from "./svglib.mjs";
import fs from "fs";
import { promisify } from "util";
import { rect } from "./svglib.mjs";
import { mask } from "./svglib.mjs";
import { storeSVG } from "./svglib.mjs";

const writeFile = promisify(fs.writeFile);

const aadamak = (color) => g([
  circle({
    cx: 0, cy: -15, r: 10, fill: color,
  }),
  g([
    line({
      x1: 0, y1: -15, x2: 0, y2: 15,
    }),
    line({
      x1: 0, y1: 0, x2: 15, y2: -5,
    }),
    line({
      x1: 0, y1: 0, x2: -15, y2: -5,
    }),
    line({
      x1: 0, y1: 15, x2: 15, y2: 30,
    }),
    line({
      x1: 0, y1: 15, x2: -15, y2: 30,
    }),
  ], {
    style: `stroke-width: 4; stroke: ${color}; stroke-linecap: round`,
  }),
]);

const polyPoint = (n, size) => {
  const pi2 = Math.PI*2;
  return [...Array(n).keys()].map(i => ({
    x: Math.cos(pi2*i/n)*size,
    y: Math.sin(pi2*i/n)*size,
  }));
};

const polyCircle = (n, stroke) => {
  return g(polyPoint(n, 40).map(({ x, y }) => move(
    scale(aadamak(stroke), 0.3, 0.3), x, y
  )));
};

const equiSign = ({ x, y, fill }) => {
  const xd = 0.7, yd = 2;
  return g([
    line({ x1: x - xd, y1: y - yd, x2: x - xd, y2: y + yd }),
    line({ x1: x + xd, y1: y - yd, x2: x + xd, y2: y + yd }),
  ], {
    style: `stroke-width: 0.5; stroke: ${fill}; stroke-linecap: round`,
  });
};

const equiRow = (n, { x, y, fill, angle = 0 }) => {
  return g([...Array(n).keys()].map(i => {
    return equiSign({ x, y: y + i*7, fill });
  }), {
    transform: `rotate(${angle}, ${x}, ${y})`,
  });
};

const doc = (stroke, fill) => {
  const x = 9, y = 13, k = 4, d = 2;
  return g([
    polygon({
      points: `0,0 0,${y} ${x},${y} ${x},${k} ${x-k},0 0,0`,
    }),
    line({ x1: x, y1: k, x2: x-k, y2: k }),
    line({ x1: x-k, y1: 0, x2: x-k, y2: k }),
    line({ x1: 2, y1: k-1, x2: x-k, y2: k-1 }),
    ...[...Array(4).keys()].map(i => {
      return line({ x1: 2, y1: k+1.2+i*d, x2: x-2, y2: k+1.2+i*d});
    })
  ], {
    style: `stroke-width: 0.8; stroke: ${stroke}; stroke-linecap: square; fill: ${fill}`,
    transform: `translate(${-x/2}, ${-y/2})`,
  });
};

const graph = (fill) => g([
  equiRow(11, { x: 15, y: 50, fill, angle: 270 }),
  equiRow(9, { x: 36, y: 74, fill, angle: 210 }),
  equiRow(9, { x: 36, y: 26, fill, angle: 330 }),
  equiRow(8, { x: 38, y: 20, fill, angle: 300 }),
  equiRow(8, { x: 20, y: 59, fill, angle: 300 }),
  equiRow(8, { x: 30, y: 23, fill, angle: 0 }),
  equiRow(8, { x: 70, y: 23, fill, angle: 0 }),
  equiRow(8, { x: 18, y: 43, fill, angle: 240 }),
  equiRow(8, { x: 39, y: 80, fill, angle: 240 }),
  equiRow(8, { x: 39, y: 80, fill, angle: 240 }),
  equiRow(4, { x: 40, y: 12, fill, angle: 270 }),
  equiRow(4, { x: 40, y: 90, fill, angle: 270 }),
  equiRow(4, { x: 12, y: 60, fill, angle: 330 }),
  equiRow(4, { x: 76, y: 20, fill, angle: 330 }),
  equiRow(4, { x: 24, y: 20, fill, angle: 30 }),
  equiRow(4, { x: 88, y: 60, fill, angle: 30 }),
]);

const logo = (stroke, fill) => {
  const myDoc = doc(stroke, fill);
  return g([
    //circle({ cx: 50, cy: 50, r: 50 }),
    move(polyCircle(6, stroke), 50, 48),
    graph(stroke),
    move(myDoc, 70, 38),
    move(myDoc, 30, 50),
    move(myDoc, 50, 90),
    //grid(),
  ]);
};

const rectLogo = (stroke, fill) => g([
  rect({ x: 0, y: 0, height: 100, width: 100, fill}),
  logo(stroke, fill),
]);

const licenseLogo = (stroke, fill, license) => g([
  rect({ x: 0, y: 0, height: 100, width: 120, fill}),
  logo(stroke, fill),
  `<text transform="translate(110,0)" style="fill:${stroke};writing-mode: vertical-rl; text-orientation: upright; font-size: 11px">${license}</text>`,
]);

const circleLogo = (stroke, fill) => g([
  circle({ cx: 50, cy: 50, r: 50, fill}),
  logo(stroke, fill),
]);

const transparentLogo = (stroke) => mask(
  logo(stroke, '#000'),
  rectLogo('#fff', '#000'),
);

const mainBackground = (stroke, fill) => {
  const leftPos = [
    [10, 20],
    [40, 15],
    [70, 30],
    [50, 50],
    [80, 60],
    [20, 70],
    [60, 80],
  ];
  const rightPos = leftPos;
  const myDoc = scale(doc(stroke, fill), 1.2);
  return g([
    rect({ x: 0, y: 0, width: 270, height: 100, fill }),
    move(logo(stroke, fill), 85, 0),
    ...leftPos.map(([x, y]) => move(myDoc, x, y)),
    ...rightPos.map(([x, y]) => move(myDoc, 180+y, x)),
  ]);
};

const og = (stroke, fill) => {
  const leftPos = [
    [10*6.3, 20*6.3],
    [40*6.3, 15*6.3],
    [30*6.3, 50*6.3],
    [20*6.3, 70*6.3],
    [40*6.3, 80*6.3],
  ];
  const rightPos = [
    [870+10*6.3, 20*6.3],
    [870+40*6.3, 15*6.3],
    [870+30*6.3, 50*6.3],
    [870+20*6.3, 70*6.3],
    [870+40*6.3, 80*6.3],
  ];;
  const myDoc = scale(doc(stroke, fill), 6.3*1.2);
  return g([
    rect({ x: 0, y: 0, width: 1200, height: 630 }),
    move(scale(logo(stroke, fill), 6.3), 600-630/2, 0),
    ...leftPos.map(([x, y]) => move(myDoc, x, y)),
    ...rightPos.map(([x, y]) => move(myDoc, x, y)),
  ]);
};

const main = async () => {
  const red = '#f44333';
  await storeSVG('output/black-red.svg', (rectLogo(red, '#000')));
  await storeSVG('output/black-red-circle.svg', (circleLogo(red, '#000')));
  await storeSVG('output/transparent-red.svg', transparentLogo(red));
  await storeSVG('output/transparent-white.svg', (transparentLogo('#fff')));
  await storeSVG('output/main-background.svg', mainBackground(red, '#000'), {
    viewBox: '0 0 270 100',
  });
  await storeSVG('output/og.svg', og(red, '#000'), {
    viewBox: '0 0 1200 630',
  });
  await storeSVG('output/cc-by-sa.svg', licenseLogo(red, '#000', 'CC BY SA'), {
    viewBox: '0 0 120 100',
  });
};

main();